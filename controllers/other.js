const connection = require("../models").connection;

const controller = {
  reset: (req, res) => {
    connection
      .sync({ force: true })
      .then(() => {
        res.status(201).send({
          message: "Database reset",
        });
      })
      .catch(() => {
        res.status(500).send({
          message: "Reset DB error",
        });
      });
  },
  hello: (req, res) => {
    res.status(200).send({ message: "Hello world!" });
  },
};

module.exports = controller;
