const logic = require("./logic");
const other = require("./other");

const controllers = {
  logic,
  other,
};

module.exports = controllers;
