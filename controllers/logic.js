//IMPORTS AS NEEDED
const UserDB = require("../models/").User;

const controller = {
  addUser: async (req, res) => {
    const user = {
      userName: req.body.userName,
      password: req.body.password,
      email: req.body.email,
      telefon: req.body.telefon,
      age: req.body.age,
    };

    await UserDB.create(user)
      .then(() => {
        res.status(200).send({ message: "User added!" });
      })
      .catch(() => {
        res.status(500).send({ message: "Server error!" });
      });

    // try {
    //   await UserDB.create(user);
    //   res.status(200).send({ message: "User added!" });
    // } catch (err) {
    //   res.status(500).send(err);
    // }
  },
  getSpecificData: async (req, res) => {
    await UserDB.findOne({
      attributes: ["email", "telefon"],
      where: {
        userName: req.params.userName,
      },
    })
      .then((user) => {
        res.status(200).send(user);
      })
      .catch(() => {
        res.status(400).send({ message: "No user found" });
      });

    // TEMA PENTRU ACAS:
    // 1. Validari pentru POST Request sa faceti validari la addUser.
    // 2. O RUTA PUT CARE SA UPDATEZE emailul si parola
    // 3. O RUTA DELETE CARE SA STEARGA TOTI USERII CU EMAILUL cezu98@gmail.com.
  },
};

module.exports = controller;
