const express = require("express");
const router = express.Router();
const userRouter = require("./userRouter");
const otherRouter = require("./other");

router.use("/", otherRouter);

router.use("/", userRouter);

module.exports = router;
