const express = require("express");
const router = express.Router();
const userController = require("../controllers/").logic;

router.post("/register", userController.addUser);
router.get("/getData/:userName", userController.getSpecificData);

module.exports = router;
