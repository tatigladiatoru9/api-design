module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "user",
    {
      // id: {
      //     type: DataTypes.INTEGER,
      //     primaryKey: true,
      //     autoIncrement: true
      // },
      userName: DataTypes.STRING,
      password: DataTypes.STRING,
      email: DataTypes.STRING,
      telefon: DataTypes.STRING,
      age: DataTypes.INTEGER,
    },
    {
      tableName: "user",
    },
    {
      underscored: true,
    }
  );
};
